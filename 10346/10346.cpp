#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int get_new_roll_count(int butts_in_hand, int butts_required) {
    return static_cast<int>(butts_in_hand / butts_required);
}

int main() {
    int n, k;
    while (scanf("%d %d", &n, &k) == 2) {
        int total = n;
        int butts = n;
        int new_rolls = 0;
        do {
            new_rolls = get_new_roll_count(butts, k);
            butts = butts % k + new_rolls;
            total += new_rolls;
        } while (new_rolls);
        printf("%d\n", total);
    }
    return 0;
}