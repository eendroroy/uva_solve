# include<stdio.h>

int main() {
    long int m, n;
    while (scanf("%ld%ld", &m, &n) == 2)
        if (m > n) printf("%ld\n", m - n);
        else printf("%ld\n", n - m);
    return 0;
}