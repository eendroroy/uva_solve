#include<iostream>

using namespace std;

int main() {
    int register tc, d, p;
    cin >> tc;
    while (tc--) {
        int register h = 0;
        cin >> d;
        cin >> p;
        int register arr[10000];
        int register t = 0;
        while (d - t) arr[t++] = 0;
        while (p--) {
            int register hp, dt = 0;
            cin >> hp;
            while (d - dt) {
                if ((dt + 1) % hp == 0) arr[dt] = 1;
                if ((dt + 1) % 7 == 0) arr[dt] = 0;
                if ((dt + 2) % 7 == 0) arr[dt] = 0;
                dt++;
            }
        }
        t = 0;
        while (d - t) if (arr[t++] == 1) h++;
        cout << h << endl;
    }
    return 0;
}
