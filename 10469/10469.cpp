#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int first = 0, second = 0;
    while(scanf("%d %d", &first, &second) == 2){
        int bits_count = 32;
        int result=0;
        while(--bits_count){
            result+=((((first>>(31-bits_count))&1)+((second>>(31-bits_count))&1))&1)<<(31-bits_count);
        }
        cout << result << endl;
    }
    return 0;
}