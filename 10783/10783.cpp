#include<iostream>

using namespace std;

int main() {
    int test_case, limit_start, limit_end, sum;
    cin >> test_case;
    for (int register i = 0; i < test_case; i++) {
        cin >> limit_start;
        cin >> limit_end;
        if (limit_start > limit_end) {
            int temp = limit_end;
            limit_end = limit_start;
            limit_start = temp;
        }
        int j = limit_start;
        sum = 0;
        while (j <= limit_end) {
            if (j % 2 == 1) sum += j;
            j++;
        }
        cout << "Case " << i + 1 << ": " << sum << endl;
    }
    return 0;
}