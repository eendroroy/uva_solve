#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

// visual c++
#define i64 __int64
#define i32 __int32
#define i16 __int16
#define i8 __int8

/*
// vc++ to gcc
#define i64 long long int
#define %I64d %lld
*/

// gcc
#define LL long long
#define U unsigned

#define pi acos(-1)

#define INF 2147483647
#define INF64 9223372036854775807

/*
#define EFS16 (2e-11)
#define EFS32 (2e-24)
#define EFS64 (2e-53)
#define EFS128 (2e-113)
*/

#define SIZE 1000
#define MAX 10000
#define MOD 10000003

int len(long int n) {
    long int c = 0;
    do {
        c++;
        if (n == 1) break;
        if (n % 2 == 0) n = n / 2;
        else if (n % 2 == 1) n = 3 * n + 1;
    } while (1);
    return static_cast<int>(c);
}


int main() {
    static long int a[1000000], i, j, t;
    static long int b[1000000], x, y;
    while (scanf("%ld%ld", &i, &j) != EOF) {
        t = i;
        x = 0;
        while (1) {
            a[x] = t;
            if (a[x] == j)
                break;
            if (t > j) t--;
            if (t < j) t++;
            x++;
        }
        for (y = 0; y <= x; y++) b[y] = len(a[y]);
        t = b[0];
        for (y = 0; y <= x; y++) if (b[y] > t) t = b[y];
        printf("%ld %ld %ld\n", i, j, t);
    }
    return 0;
}
