#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

string get_spaces(int number) {
    if (number < 10) return "  ";
    else return " ";
}

int next(bool array[], int current, int increment, int size) {
    for (int i = 0; i < increment; i++) {
        current++;
        if (current >= size) current -= size;
        while (!array[current]) {
            current++;
            if (current >= size) current -= size;
        }
    }
    return current;
}

int previous(bool array[], int current, int decrement, int size) {
    for (int i = 0; i < decrement; i++) {
        current--;
        if (current < 0) current += size;
        while (!array[current]) {
            current--;
            if (current < 0) current += size;
        }
    }
    return current;
}

int main() {
    int N = 0, k = 0, m = 0;
    scanf("%d %d %d", &N, &k, &m);
    while (N != 0 && k != 0 && m != 0) {
        bool array[20];
        for (int i = 0; i < N; i++) array[i] = true;

        int start = -1, end = N, cut_off_count = 0;

        while (1) {
            if (cut_off_count < N) {
                start = next(array, start, k, N);
                end = previous(array, end, m, N);
            }
            if (cut_off_count >= N - 1) break;
            if (start == end) {
                cout << get_spaces(start + 1) << start + 1;
                cut_off_count += 1;
            } else {
                cout << get_spaces(start + 1) << start + 1 << get_spaces(end + 1) << end + 1;
                cut_off_count += 2;
            }

            array[start] = false;
            array[end] = false;

            if (cut_off_count < N) cout << ",";
        }

        if (cut_off_count < N) {
            if (start == end) {
                cout << get_spaces(start + 1) << start + 1;
            } else {
                cout << get_spaces(start + 1) << start + 1 << get_spaces(end + 1) << end + 1;
            }
        }
        cout << endl;
        scanf("%d %d %d", &N, &k, &m);
    }
    return 0;
}
