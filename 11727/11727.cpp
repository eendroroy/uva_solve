#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int test_cases = 0, count_cases = 0;
    cin >> test_cases;
    while (count_cases++ < test_cases) {
        int max = 0;
        int min = 10001;
        int survive = 0;
        int current = 0;
        int count = 3;
        while (count--) {
            cin >> current;
            if (current > max) {
                survive = max;
                max = current;
            }
            if (current < min) {
                survive = min;
                min = current;
            }
            if (current > min && current < max) survive = current;
        }
        cout << "Case " << count_cases << ": " << survive << endl;
    }
    return 0;
}