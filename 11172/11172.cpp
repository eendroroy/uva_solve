#include<iostream>

using namespace std;

int main() {
    long double a, b;
    int t;
    cin >> t;
    for (int register i = 0; i < t; i++) {
        cin >> a;
        cin >> b;
        if (a == b) cout << '=' << endl;
        if (a > b) cout << '>' << endl;
        if (a < b) cout << '<' << endl;
    }
    return 0;
}
