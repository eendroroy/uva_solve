#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <map>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    map<char, char> keyboard;
    keyboard['B'] = 'V';
    keyboard['C'] = 'X';
    keyboard['D'] = 'S';
    keyboard['E'] = 'W';
    keyboard['F'] = 'D';
    keyboard['G'] = 'F';
    keyboard['H'] = 'G';
    keyboard['I'] = 'U';
    keyboard['J'] = 'H';
    keyboard['K'] = 'J';
    keyboard['L'] = 'K';
    keyboard['M'] = 'N';
    keyboard['N'] = 'B';
    keyboard['O'] = 'I';
    keyboard['P'] = 'O';
    keyboard['R'] = 'E';
    keyboard['S'] = 'A';
    keyboard['T'] = 'R';
    keyboard['U'] = 'Y';
    keyboard['V'] = 'C';
    keyboard['W'] = 'Q';
    keyboard['X'] = 'Z';
    keyboard['Y'] = 'T';

    keyboard['['] = 'P';
    keyboard[']'] = '[';
    keyboard['\\'] = ']';
    keyboard['\''] = ';';
    keyboard[';'] = 'L';
    keyboard[','] = 'M';
    keyboard['.'] = ',';
    keyboard['/'] = '.';
    keyboard['\n'] = '\n';

    keyboard['1'] = '`';
    keyboard['2'] = '1';
    keyboard['3'] = '2';
    keyboard['4'] = '3';
    keyboard['5'] = '4';
    keyboard['6'] = '5';
    keyboard['7'] = '6';
    keyboard['8'] = '7';
    keyboard['9'] = '8';
    keyboard['0'] = '9';
    keyboard['-'] = '0';
    keyboard['='] = '-';
    keyboard['\b'] = '=';
    keyboard[' '] = ' ';
    keyboard['A'] = 'A';
    keyboard['Q'] = 'Q';
    keyboard['Z'] = 'Z';
    char input = static_cast<char>(getchar());
    while (input != EOF) {
        cout << keyboard[input];
        input = static_cast<char>(getchar());
    }
    return 0;
}
