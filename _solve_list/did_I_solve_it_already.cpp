#include<iostream>
#include <vector>

int main() {
    std::vector<int> solve_list = std::vector<int>({
            100, 102, 119, 133, 136, 272, 492, 10019, 10050,
            10055, 10346, 10370, 10783, 11172, 10082, 10591,
            11727, 10469
    });
    check:
    int problem_number = 0;
    bool solved_it = false;
    std::cout << "Problem Number?" << std::endl;
    std::cin >> problem_number;
    for (auto i:solve_list) {
        if (i == problem_number) {
            solved_it = true;
            break;
        }
    }
    if (solved_it) std::cout << "Solved it already." << std::endl;
    else std::cout << "Didn't solved it yet." << std::endl;
    std::cout << "Check another Problem[y/n]?" << std::endl;
    char check_again = 'n';
    std::cin >> check_again;
    if (check_again == 'y') goto check;
    return 0;
}