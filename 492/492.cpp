#include<cstdio>
#include<set>

char input, start_input;

bool alphabetic(char character) {
    return ((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z'));
}

std::set<char> vowels = std::set<char>({'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u'});

bool vowel(char character) {
    return (vowels.find(character) != vowels.end());
}

int main() {
    while ((input = static_cast<char>(getchar())) != EOF) {
        if (alphabetic(input)) {
            start_input = input;
            if (vowel(input)) printf("%c", input);
            while (alphabetic(input = static_cast<char>(getchar())))
                printf("%c", input);
            if (input != '\n') {
                if (vowel(start_input)) printf("ay");
                else printf("%cay", start_input);
            }

        }
        printf("%c", input);
    }
    return 0;
}