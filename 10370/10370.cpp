#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

float get_average(int array[], int size) {
    int total = 0;
    for (int i = 0; i < size; i++) {
        total += array[i];
    }
    return static_cast<float>(total) / static_cast<float>(size);
}

int get_above_average_count(int array[], int size, float average) {
    int above_average = 0;
    for (int i = 0; i < size; i++) {
        if (array[i] > average) above_average++;
    }
    return above_average;
}

int main() {

    int test_caese = 0;
    scanf("%d", &test_caese);
    while (test_caese--) {
        int array[1001] = {0};
        int number_of_students = 0;
        int number_of_students_above_average = 0;
        scanf("%d", &number_of_students);
        for (int i = 0; i < number_of_students; i++) {
            scanf("%d", array + i);
        }
        number_of_students_above_average = get_above_average_count(array, number_of_students, get_average(array, number_of_students));
        printf("%.3f%\n", static_cast<float>(number_of_students_above_average * 100.0) / static_cast<float>(number_of_students));
    }

    return 0;
}