#include<iostream>
#include<string.h>

using namespace std;
#define S 50

int arr[S];

void dec_bin(int x, int i = 0) {
    while (x > 0) {
        arr[i++] = x % 2;

        x = x / 2;
    }
    arr[i] = 2;
}

void hex_bin(int x) {
    int i = 0;
    while (x > 0) {
        int temp = x % 10, l = 0;
        while (1) {
            if (l == 4) break;
            arr[i++] = temp % 2;
            l++;
            temp = temp / 2;
        }
        x = x / 10;
    }
    arr[i] = 2;
}

int count() {
    int c = 0, i = 0;
    while (arr[i] != 2) if (arr[i++] == 1) c++;
    return c;
}


int main() {
    int cas;
    cin >> cas;
    while (cas--) {
        int num;
        cin >> num;
        dec_bin(num);
        cout << count();
        hex_bin(num);
        cout << ' ' << count() << endl;
    }
    return 0;
}