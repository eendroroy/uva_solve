#include<stdio.h>

#define S 9

int main() {
    long long int arr[S];
    while (1) {
        if (scanf("%lld%lld%lld%lld%lld%lld%lld%lld%lld", &arr[0], &arr[1], &arr[2], &arr[3], &arr[4], &arr[5], &arr[6], &arr[7], &arr[8]) == EOF) break;
        int i = 0;
        long long int arr2[6];
        arr2[0] = arr[3] + arr[6] + arr[2] + arr[8] + arr[1] + arr[4];
        arr2[1] = arr[3] + arr[6] + arr[1] + arr[7] + arr[2] + arr[5];
        arr2[2] = arr[5] + arr[8] + arr[0] + arr[6] + arr[1] + arr[4];
        arr2[3] = arr[5] + arr[8] + arr[1] + arr[7] + arr[0] + arr[3];
        arr2[4] = arr[4] + arr[7] + arr[0] + arr[6] + arr[2] + arr[5];
        arr2[5] = arr[4] + arr[7] + arr[2] + arr[8] + arr[0] + arr[3];
        i = 0;
        int min = 0;
        while (i < 6) {
            if (arr2[i] < arr2[min]) min = i;
            i++;
        }
        switch (min) {
            case 0:
                printf("BCG %lld\n", arr2[min]);
                continue;
            case 1:
                printf("BGC %lld\n", arr2[min]);
                continue;
            case 2:
                printf("CBG %lld\n", arr2[min]);
                continue;
            case 3:
                printf("CGB %lld\n", arr2[min]);
                continue;
            case 4:
                printf("GBC %lld\n", arr2[min]);
                continue;
            case 5:
                printf("GCB %lld\n", arr2[min]);
                continue;
        }
    }
    return 0;
}