#include <ctime>
#include <cmath>
#include <cctype>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cstring>

#include <map>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>

using namespace std;

int get_next(int number) {
    int squire_sum = 0;
    while (number) {
        squire_sum += pow(number % 10, 2);
        number = number / 10;
    }
    return squire_sum;
}

int main() {
    int test_cases = 0, count_case = 0;
    cin >> test_cases;
    while (count_case++ < test_cases) {
        int number = 0;
        cin >> number;
        map<int, bool> track;
        int next = get_next(number);
        track[1] = true;
        track[number] = true;
        while (!track[next]) {
            track[next] = true;
            next = get_next(next);
        }
        if (next == 1) cout << "Case #" << count_case << ": " << number << " is a Happy number." << endl;
        else cout << "Case #" << count_case << ": " << number << " is an Unhappy number." << endl;
    }
    return 0;
}